from django.db import models
# Create your models here.


class Notes(models.Model):
    id_note = models.BigAutoField(primary_key=True)
    title = models.CharField(max_length=50)
    body = models.TextField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)

    class Meta:
        db_table = "notes"
        verbose_name = "note"
        verbose_name_plural = "notes"

    def __str__(self) -> str:
        return self.title
