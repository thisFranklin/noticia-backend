from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework import status
from rest_framework.response import Response
from noticia.models import Notes
from noticia.serializers import NoteSerialiser

# Create your views here.

@api_view(["GET", "POST"])
@permission_classes([IsAuthenticated])
def notes_list(request):
    if request.method == "GET":
        notes = Notes.objects.all()
        serializer = NoteSerialiser(notes, many=True)
        return Response(serializer.data)
    
    elif request.method == "POST":
        serializer = NoteSerialiser(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(["POST"])
@permission_classes([AllowAny])
def login_page(request):
    print("Hello")
    pass